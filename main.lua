GameState = require "lib.hump.gamestate"
vector = require "lib.hump.vector"
Class = require "lib.hump.class"

--States
local menu = {}
local game = {}

--Screen stuff
local screenWidth = love.graphics.getWidth()
local screenHeight = love.graphics.getHeight()
local size = 10

local boxSize = 10
local width = ( screenWidth / boxSize ) - 1
local height = ( screenHeight / boxSize ) - 1 - 5 -- -5 for GUI

local roundTime = 30
local enemySpeed = 3
local enemySprintSpeed = 12.35
local playerSpeed = 12
local playerSprintSpeed = 18
local enemyVisibleTime = 1
local playerSprintConsumption = 20
local upgradeList = { "Max Health", "Max Sprint", "Sprint Regen" }

local bufferTime = 0.3

local level
local upgrades
local pointsSpent
local totalFood
local totalHit

--Audio
local bgm = love.audio.newSource( "audio/bgm.ogg" )
bgm:setLooping( true )
local fx_cursor = love.audio.newSource( "audio/cursor.ogg", "static" )
local fx_wrong = love.audio.newSource( "audio/wrong.ogg", "static" )
local fx_powerup = love.audio.newSource( "audio/powerup.ogg", "static" )
local fx_enter = love.audio.newSource( "audio/enter.ogg" )
local fx_death = love.audio.newSource( "audio/death.ogg", "static" )

local fx_spot = {}
fx_spot[ 1 ] = love.audio.newSource( "audio/spot1.ogg", "static" )
fx_spot[ 2 ] = love.audio.newSource( "audio/spot2.ogg", "static" )
fx_spot[ 3 ] = love.audio.newSource( "audio/spot3.ogg", "static" )
fx_spot[ 4 ] = love.audio.newSource( "audio/spot4.ogg", "static" )

local lastSpot = -1

--Audio adjustments
fx_powerup:setVolume( 0.5 )


function initValues()
	upgrades = { [1] = 0, [2] = 0, [3] = 0, [4] = 0 }
	pointsSpent = 0
	totalFood = 0
	level = 0
	totalHit = 0
end

function playSpot()
	local idx = -1
	repeat
		idx = math.random( 1, #fx_spot )
	until idx ~= lastSpot
	lastSpot = idx
	love.audio.play( fx_spot[ idx ] )
end

function calculateAvailablePoints()
	--max of 5 per upgrade
	return math.min( math.floor( totalFood / 4 ), ( #upgradeList * 5 ) ) - pointsSpent
end

--Game stuff
Enemy = Class {
	init = function( self, position )
		self.position = position
		self.targetPosition = position
		self.state = "idle"
		self.visibleTime = 0.0
	end
}

function Enemy:setTargetPosition( position, isActive )
	self.targetPosition = position
	
	if ( isActive == nil ) then
		isActive = false
	end
	
	if ( isActive ) then
		if ( self.state ~= "active" ) then
			playSpot()
			self.state = "active"
		end
	else
		self.state = "walking"
	end
end

function Enemy:update( dt )
	local distance = self.position:dist( self.targetPosition )
	if ( distance < 0.3 ) then
		self.state = "idle"
	else
		--Set speed
		local speed = enemySpeed
		if ( self.state == "active" ) then
			speed = enemySprintSpeed
		end
		
		--Do movement
		local movement = ( self.targetPosition - self.position ):normalized()
		self.position = self.position + ( movement * dt * speed )
	end
	
	if ( self.visibleTime > 0 ) then
		self.visibleTime = math.max( self.visibleTime - dt, 0 )
	end
end

function Enemy:setSpotted()
	self.visibleTime = enemyVisibleTime
end

function Enemy:getVisibility()
	return self.visibleTime / enemyVisibleTime
end

Color = Class {
	init = function( self, r, g, b, a )
		self.r = r
		self.g = g
		self.b = b
		if ( a ~= nil ) then
			self.a = a
		else
			self.a = 1.0
		end
	end
}

function Color.clamp( val )
	return math.max( math.min( val, 255 ), 0 )
end

function Color:add( color )
	if ( color == nil ) then
		return Color( self.r, self.g, self.b, self.a )
	end
	
	local r = Color.clamp( ( self.r * self.a ) + ( color.r * color.a ) )
	local g = Color.clamp( ( self.g * self.a ) + ( color.g * color.a ) )
	local b = Color.clamp( ( self.b * self.a ) + ( color.b * color.a ) )
	local a = math.max( self.a, color.a )
	return Color( r, g, b, a )
end

function Color:multiply( color )
	if ( color == nil ) then
		return Color( self.r, self.g, self.b, self.a )
	end

	local r = Color.clamp( ( self.r * self.a ) * ( color.r * color.a ) )
	local g = Color.clamp( ( self.g * self.a ) * ( color.g * color.a ) )
	local b = Color.clamp( ( self.b * self.a ) * ( color.b * color.a ) )
	local a = self.a * color.a
	return Color( r, g, b, a )
end

function Color:withVisibility( visibility )
	local r = Color.clamp( self.r * self.a * visibility )
	local g = Color.clamp( self.g * self.a * visibility )
	local b = Color.clamp( self.b * self.a * visibility )
	local a = self.a * visibility
	return Color( r, g, b, a )
end


local colors = {}
-- colors[ "flashlight" ] = Color( 253, 213, 128, 0.5 )
colors[ "flashlight" ] = Color( 223, 183, 98, 0.8 )
colors[ "flashlightFalloff" ] = Color( 180, 130, 90, 0.5 )
colors[ "near" ] = Color( 40, 40, 40, 0.5 )
colors[ "nearFalloff" ] = Color( 30, 30, 30, 0.5 )
colors[ "dark" ] = Color( 0, 0, 0, 0.0 )
colors[ "food" ] = Color( 40, 100, 40, 1.0 )
colors[ "player" ] = Color( 30, 40, 230 )
colors[ "enemy" ] = Color( 180, 40, 20 )

local function setColor( color )
	love.graphics.setColor( color.r, color.g, color.b )
end

function menu:enter()
	self.target = vector( screenWidth * 0.35, screenHeight * 0.4 )
	self.positions = {}	
	self.times = {}
	self.sizes = {}
	self.rotations = {}
	local i
	for i = 0, 4 do
		self.positions[ i ] = self.target
		self.times[ i ] = math.random( 2, 50 ) / 100.0
		self.sizes[ i ] = math.random( 75, 400 ) / 100.0
		self.rotations[ i ] = math.random( -25, 25 ) / 100
	end

end

function menu:update( dt )
	local i
	for i = 1, #self.times do
		local current = self.times[ i ]
		current = current - dt
		if ( current <= 0 ) then
			current = math.random( 2, 50 ) / 100.0
			local xRandom = math.random( -400, 400 ) / 10.0
			local yRandom = math.random( -400, 400 ) / 10.0
			self.positions[ i ] = vector( self.target.x + xRandom, self.target.y + yRandom )
			self.sizes[ i ] = math.random( 75, 600 ) / 100.0
			self.rotations[ i ] = math.random( -25, 25 ) / 100
		end
		self.times[ i ]  = current
	end
end

--Define the states
function menu:draw()
	--Clear with white
	love.graphics.setColor( 0, 0, 0 )
	love.graphics.rectangle( "fill" , 0, 0, 900, 600 )
	love.graphics.setColor( 70, 70, 70 )
	love.graphics.print( "By Richard Locsin - LD26", screenWidth * 0.90, screenHeight * 0.95, 0, 1, 1, 100 )
	love.graphics.setColor( 255, 255, 255 )
	love.graphics.print( "Press [Space] to start...", screenWidth * 0.5, screenHeight * 0.8, 0, 1, 1, 100 )
	

	local i
	for i = 1, #self.positions do
		local color = math.random( 20, 210 )
		love.graphics.setColor( color, color, color )
		love.graphics.print( "Come No Closer", 
			self.positions[ i ].x, self.positions[ i ].y, self.rotations[ i ], 
			self.sizes[ i ], self.sizes[ i ], self.sizes[ i ] * 4, self.sizes[ i ] * 1 )
	end
end

function menu:keypressed( key )
    if ( key == "escape" ) then
		love.event.push( "quit" )
    elseif ( key == " " ) then
    	love.audio.play( fx_enter )
		GameState.switch( game )
    end
end

function game:enter()
    level = 0
    game:startLevel()    
end

function game:startLevel()
	level = level + 1
	self.time = 0
    self.foodEaten = 0
    self.cursor = 0
    self.timesHit = 0
    self.enemyHit = math.min( 45, (level + 1 ) * 3 )
    self.enemySight = math.min( 12, 6 + math.floor( level / 10 ) )
    self.paused = false
	self.player = {}
	self.player.position = vector( width / 2, height / 2 )
	self.player.staminaMax = math.floor( 50 * ( ( upgrades[ 2 ] + 1 ) / 6 ) )
	self.player.stamina = self.player.staminaMax
	self.player.staminaLastCharge = -10
	self.player.staminaRegen = math.floor( 30 * ( ( upgrades[ 3 ] + 1 ) / 6 ) )
	self.player.healthMax = math.floor( 100 *  ( ( upgrades[1] + 1 ) / 6 ) )
	self.player.health = self.player.healthMax
	self.mouse = {}
	self.mouse.position = vector( 450, 300 )
	self.player.flashlight = true
	self:generateFood()
	self.enemies = {}
	self.state = "game"
	
	local i
	for i = 1, level do
		game:createEnemy()
	end
end

function game:generateFood()
	local position
	
	repeat
		position = vector( math.random( 0, width ), math.random( 0, height) )
	until self.player.position:dist( position ) > 20 
	
	self.food =  position
end


function game:generateEnemyPosition()
	local position
	repeat
		position = vector( math.random( 0, width ), math.random( 0, height) )
	until ( self.player.position:dist( position ) > 15 and self.food:dist( position ) > 3 )
	return position
end

function game:createEnemy()
	local position = self:generateEnemyPosition()
	local enemy = Enemy( position )
	self.enemies[ #self.enemies + 1 ] = enemy
end

function game:shouldGameUpdate()
	if ( not self.paused and self.time <= roundTime ) then
		return true
	else
		return false
	end
end


function game:update( dt )
	if ( self.state == "game" ) then
		if ( not self.paused ) then 
			self.time = self.time + dt	
		
			if ( self.time <= roundTime ) then
				self:doPlayerInput( dt )
				self:updatePlayer( dt )
				self:updateEnemies( dt )
			elseif (self.state == "game" ) then
				self:doWin()
				self.state = "win"
			end
		end
	else
		self.paused = false
		self.bufferTime = self.bufferTime - dt
	end
end

function drawBox( x, y )
	love.graphics.rectangle( "fill", x * boxSize, y * boxSize, boxSize, boxSize )
end


function game:getEnemyAt( x, y )
	local i
	for i = 1, #self.enemies do
		local enemy = self.enemies[ i ]
		if ( x == math.floor( enemy.position.x ) and y == math.floor( enemy.position.y ) ) then
			return enemy
		end
	end
	return nil
end

function game:doWin()
	if ( self.state ~= "win" ) then
		self.bufferTime = bufferTime
		self.state = "win"
	end
end

function game:doLose()
	if ( self.state ~= "lose" ) then
		self.bufferTime = bufferTime
		self.state = "lose"
		love.audio.play( fx_death )
	end
end

function game:draw()
	--Clear with black
	love.graphics.setColor( 0, 0, 0 )
	love.graphics.rectangle( "fill" , 0, 0, 900, 600 )
	love.graphics.setColor( 0, 0, 0 )
	
	local x = 0
	local y = 0
	local visibleRange = 20

	local mouse = vector( self.mouse.position.x / boxSize, self.mouse.position.y / boxSize )
	local direction = ( mouse - self.player.position):normalized()

    for x = 0, width do
        for y = 0, height do
		
			local current = vector( x + 0.5, y + 0.5 )
			--Default dark
			local color = nil
		
			local distance = self.player.position:dist( current )
			
			local enemy = self:getEnemyAt( x, y )
			local setEnemyVisible = false
			
			if ( distance < visibleRange ) then
				
				if (distance < ( visibleRange / 5 )  ) then
					color = colors[ "near" ]
					setEnemyVisible = true
				elseif (distance < ( visibleRange / 4 ) ) then
					color = colors[ "nearFalloff" ]
					setEnemyVisible = true
				end				
				
				if ( self.player.flashlight ) then
					local forward = (current - self.player.position):normalized()
					local angle = math.acos( direction * forward )
					if ( angle < 0.3 ) then
						color = colors[ "flashlight" ]
						setEnemyVisible = true
					elseif ( angle < 0.4 ) then
						color = colors[ "flashlightFalloff" ]
						setEnemyVisible = true
					end
				end
			end

			--If there's food here			
			if ( x == self.food.x and y == self.food.y ) then
				color = ( colors[ "food" ] ):add( color )
			end
			
			--Enemy was spotted
			if ( setEnemyVisible and enemy ~= nil ) then
				enemy:setSpotted()
			end

			--If there's food, priority goes to enemy, not food...
			if ( enemy ~= nil ) then
				local enemyVisibility = enemy:getVisibility()
				if ( enemyVisibility > 0 ) then
					color = colors[ "enemy"]:withVisibility( enemyVisibility )
				end
			end

			if ( color ~= nil ) then
				setColor( color )
				drawBox( x, y )
			end
			
			
	        --love.graphics.rectangle( "fill", x * boxSize, y * boxSize, boxSize, boxSize )    
        end
    end 
	
	--DEBUG draw all enemies
	--[[
	setColor( colors[ "enemy" ] )
	local i;
	for i = 1, #self.enemies do
		local enemy = self.enemies[ i ]
		drawBox( 
			math.floor( enemy.position.x ), 
			math.floor( enemy.position.y ) )
	end
	--]]--
	
	--Draw player (we're just drawing over. Easier to do. Hahaha! )
	setColor( colors[ "player" ] )
	drawBox( 
		math.floor( self.player.position.x ), 
		math.floor( self.player.position.y ) )
		
	game:drawGui()
end

function game:drawGui()
	local y = screenHeight - ( size * 5 )
	
	--Frame
	love.graphics.setColor( 40, 40, 40 )
	love.graphics.rectangle( "fill", 0, y, screenWidth, 5 * size )
	
	--Health
	local healthWidth = ( screenWidth / 3 ) * ( self.player.healthMax / 100 )
	love.graphics.setColor( 120, 120, 120 )
	love.graphics.rectangle( "fill", screenWidth * 0.05, y + size, healthWidth, size )
	
	love.graphics.setColor( 0, 255, 0 )
	love.graphics.rectangle( "fill", screenWidth * 0.05, y + size, healthWidth * ( self.player.health / self.player.healthMax ), size )
	
	--Stamina
	local staminaWidth = ( screenWidth / 3 ) * ( self.player.staminaMax / 50 )
	love.graphics.setColor( 120, 120, 120 )
	love.graphics.rectangle( "fill", screenWidth * 0.05, y + ( 3 * size), staminaWidth, size )
	
	love.graphics.setColor( 255, 255, 0 )
	love.graphics.rectangle( "fill", screenWidth * 0.05, y + ( 3 * size), staminaWidth * ( self.player.stamina / self.player.staminaMax ), size )
	
	
	local points = calculateAvailablePoints() 
	if ( points >= 0 and pointsSpent < 20) then
		love.graphics.setColor( 255, 255, 255 )
		love.graphics.print( "Upgrade Points: " .. points, screenWidth * 0.75, screenHeight * 0.93 )
	end
	
	if ( points + pointsSpent < #upgradeList * 5 ) then	
		local xpWidth = screenWidth / 6
		love.graphics.setColor( 120, 120, 120 )
		love.graphics.rectangle( "fill", screenWidth * 0.75, screenHeight * 0.97, xpWidth, size )
		
		local xp = ( totalFood / 4 )
		local progress = xp % 1
	
		love.graphics.setColor( 8, 101, 219 )
		love.graphics.rectangle( "fill", screenWidth * 0.75, screenHeight * 0.97, xpWidth * progress, size )
	end
	
	
	--Pause display
	love.graphics.setColor( 255, 255, 255 ) 
	if ( self.paused ) then
		love.graphics.print( "Paused", screenWidth / 2, screenHeight * 0.8, 0, 2, 2, 25 )
		love.graphics.print( "Press P to Unpause", screenWidth / 2, screenHeight * 0.85, 0, 1.5, 1.5, 65 )
	end
	
	local formattedTime = string.format( "%1.2f", math.max( roundTime - self.time, 0.0 ) )
	
	love.graphics.print( "Time: " .. formattedTime, screenWidth * 0.5, screenHeight * 0.93, 0, 1.1, 1.1 )
	love.graphics.print( "Supply: " .. self.foodEaten, screenWidth * 0.5, screenHeight * 0.96, 0, 1.1, 1.1 )
	
	if ( self.state == "win" ) then
		self:drawWin()
	elseif ( self.state == "lose" ) then
		self:drawLose()
	end
end


function game:drawWin()
	love.graphics.setColor( 85, 164, 47 )
	love.graphics.rectangle( "fill" , screenWidth * 0.2, screenHeight * 0.2, screenWidth * 0.6, screenHeight * 0.6 )
	love.graphics.setColor( 255, 255, 255 )
	love.graphics.print( "End of Day " .. level, screenWidth * 0.3, screenHeight * 0.25, 0, 1.3, 1.3 )
	love.graphics.print( "Supply Collected: " .. self.foodEaten .. "  (Total: " .. totalFood .. ")", 
		screenWidth * 0.35, screenHeight * 0.30, 0, 1, 1 )
	love.graphics.print( "Times hurt: " .. self.timesHit .. "  (Total: " .. totalHit .. ")", 
		screenWidth * 0.35, screenHeight * 0.33, 0, 1, 1 )
	
	local pointsAvailable = calculateAvailablePoints()
	love.graphics.print( "Available Upgrade Points: " .. pointsAvailable, 
		screenWidth * 0.35, screenHeight * 0.45, 0, 1, 1 )
	
	local yNext = screenHeight * 0.04
	--draw upgrades list
	local i
	for i = 1, #upgradeList do
		local points = upgrades[ i ]
		local suffix = ""
		if ( points >= 5 ) then
			suffix = " [MAX]"
		end
		love.graphics.print( upgradeList[ i ] .." : " .. points .. suffix, 
			screenWidth * 0.40, 
			( screenHeight * 0.48 ) + ( i * yNext ), 
			0, 1, 1  )
	end
	
	--Display the cursor
	if ( pointsAvailable > 0 ) then
		love.graphics.print( "Use [Space] or [Enter] to confirm", screenWidth * 0.35, screenHeight * 0.48, 0, 1, 1 )
		
		local xOffset = screenWidth * 0.38
		local yOffset = screenHeight * 0.48 + ( self.cursor * yNext )
		
		if ( self.cursor == 0 ) then
			xOffset = screenWidth * 0.33
			yOffset = screenHeight * 0.45
		end
		
		local vertices = { 
			xOffset + 0, yOffset + 1, 
			xOffset + 10, yOffset + 6, 
			xOffset + 0, yOffset + 11 }
		love.graphics.polygon( "fill", vertices )
	else
		love.graphics.print( "Press [Space] to continue...", screenWidth * 0.3, screenHeight * 0.7, 0, 1.3, 1.3 )
	end
end

function game:drawLose()
	love.graphics.setColor( 164, 85, 47 )
	love.graphics.rectangle( "fill" , screenWidth * 0.2, screenHeight * 0.2, screenWidth * 0.6, screenHeight * 0.6 )
	love.graphics.setColor( 255, 255, 255 )
	love.graphics.print( "Died on Day " .. level, screenWidth * 0.3, screenHeight * 0.25, 0, 1.3, 1.3 )
	love.graphics.print( "Supply Collected: " .. self.foodEaten .. "  (Total: " .. totalFood .. ")", 
		screenWidth * 0.35, screenHeight * 0.30, 0, 1, 1 )
	love.graphics.print( "Times hurt: " .. self.timesHit .. "  (Total: " .. totalHit .. ")", 
		screenWidth * 0.35, screenHeight * 0.33, 0, 1, 1 )
	
	local yNext = screenHeight * 0.04
	--draw upgrades list
	local i
	for i = 1, #upgradeList do
		love.graphics.print( upgradeList[ i ] .." : " .. upgrades[ i ], 
			screenWidth * 0.40, 
			( screenHeight * 0.48 ) + ( i * yNext ), 
			0, 1, 1  )
	end

	love.graphics.print( "Press [Space] to retry...", screenWidth * 0.3, screenHeight * 0.7, 0, 1.3, 1.3 )
end

function game:doPlayerInput(dt)
	local movement = vector( 0, 0 )
	--X-axis
	if love.keyboard.isDown('left') or love.keyboard.isDown( 'a' ) then
        movement.x = -1
    elseif love.keyboard.isDown('right') or love.keyboard.isDown( 'd' ) then
        movement.x =  1
    end
    --Y-axis
    if love.keyboard.isDown('up') or love.keyboard.isDown( 'w' ) then
        movement.y = -1
    elseif love.keyboard.isDown('down') or love.keyboard.isDown( 's' ) then
        movement.y =  1
    end
	
	--Sprint
	local speed = playerSpeed
	if ( love.keyboard.isDown( 'lshift' ) or love.keyboard.isDown( 'rshift' ) ) then
		if ( self.player.stamina > 0 ) then
			speed = playerSprintSpeed
			local newStamina = self.player.stamina - ( dt * playerSprintConsumption )
			self.player.stamina = math.max( newStamina, 0 )
			self.player.staminaLastCharge = self.time
		end
	end
	
	if ( self.player.stamina < self.player.staminaMax and self.time - self.player.staminaLastCharge >= 1 ) then
		local newStamina = self.player.stamina + ( dt * self.player.staminaRegen )
		self.player.stamina = math.min( newStamina, self.player.staminaMax )
	end

	--Actual movement		
	movement = movement:normalized()
	local position = self.player.position + ( movement * dt * speed )
	--clamp
	local clamp = function( low, high, n ) return math.min(math.max(n, low), high) end
	position.x = clamp( 0, width, position.x )
	position.y = clamp( 0, height, position.y )
	self.player.position = position
	
	self.mouse.position.x = love.mouse.getX()
	self.mouse.position.y = love.mouse.getY()
end

function game:updatePlayer(dt)
	--Health decrease
	local newHealth = self.player.health - ( dt * 1 )
	newHealth = math.max( 0, newHealth )
	
	--Food
	if ( self.player.position:dist( self.food ) < 3 ) then
		self:generateFood()
		newHealth = newHealth + 10
		self.foodEaten = self.foodEaten + 1
		totalFood = totalFood + 1
		newHealth = math.min( newHealth, self.player.healthMax )
		
		if ( self.foodEaten % 3 == 0 ) then
			self:createEnemy()
		end
	end
	self.player.health = newHealth
	
	if ( self.player.health == 0 ) then
		self:doLose()
	end
end

function game:resetEnemy( enemy )
	enemy.state = "idle"
	enemy.visibleTime = 0.0
	enemy.position = self:generateEnemyPosition()
	enemy:setTargetPosition( self:generateEnemyPosition() )
end

function game:updateEnemies( dt )
	local i
	for i = 1, #self.enemies do
		local enemy = self.enemies[ i ]

		--Set new target position if idling		
		if ( enemy.state == "idle" ) then
			local position = self:generateEnemyPosition()
			enemy:setTargetPosition( position )
		end
		
		--Check if near player
		local dist = enemy.position:dist( self.player.position )
		if ( dist < 0.5 ) then
			--Reset enemy & Decrease player health
			self:resetEnemy( enemy )
			self.player.health = math.max( self.player.health - self.enemyHit, 0 )
			self.timesHit = self.timesHit + 1
			totalHit = totalHit + 1
		elseif (dist < self.enemySight ) then
			--Set the enemy to chase!
			enemy:setTargetPosition( self.player.position, true )
		end
		--local newHealth = self.player.health - ( dt * 1 )
		--newHealth = math.max( 0, newHealth )
	
		enemy:update( dt )
	end
end

function game:keypressed( key )
	if ( self.state == "game" ) then 
		if ( key == "p" ) then
			self.paused = not self.paused
		end

		if ( not self.paused ) then
			if ( key == " " or key == "return" ) then
				self.player.flashlight = not self.player.flashlight
			end
		end
	elseif ( self.state == "win" ) then
		if ( self.bufferTime > 0 ) then return end
		
		local cursor = self.cursor
		if ( calculateAvailablePoints() > 0 ) then
			if ( key == 'up' ) or ( key == 'w' ) then
				cursor = cursor - 1
				self.cursor = math.max( 1, cursor )
				love.audio.play( fx_cursor )
			elseif ( key == 'down' ) or ( key == 's' ) then 
				cursor = cursor + 1
				self.cursor = math.min( #upgradeList, cursor )
				love.audio.play( fx_cursor )
			end
		end
		
		if ( key == " " ) or ( key == "return" ) then
			if ( calculateAvailablePoints() > 0 ) then
				if ( self.cursor ~= 0 and upgrades[ self.cursor ] < 5  ) then
					pointsSpent = pointsSpent + 1
					upgrades[ self.cursor ] = upgrades[ self.cursor ] + 1
					love.audio.play( fx_powerup )
				else
					love.audio.play( fx_wrong )
				end
			else
				game:startLevel()
			end
		end
	elseif ( self.state == "lose" ) then
		if ( self.bufferTime > 0 ) then return end
		
		if ( key == " " ) or ( key == "return" ) then
			initValues()
			game:startLevel()
		end
	end
end

--Load it up!
function love.load()
	love.audio.play( bgm )
	initValues()
    GameState.registerEvents()
    GameState.switch( menu )
end